import { Body, Controller, Get, Post, Req,Param, Delete } from '@nestjs/common';
import { Movie } from './entities/movie.entity';
// import { Movie } from './entities/movie.entity';
import { MovieService } from './movie.service';

@Controller('app/movies')
export class MovieController {

    constructor(private movieService: MovieService){}

    @Get()
    async showMovie(){
        try{
            const movies = await this.movieService.findAll();
            return movies;
        } catch(e){
            throw new Error("Some bad, get all");
        }
    }

    @Post()
    async saveMovie(@Body() movie:Movie){
        try{
            const savedMovie = await this.movieService.createMovie(movie);
            return savedMovie;
        } catch(e){
            throw new Error("Some bad while save");
        }
    }

    @Get('id/:id')
    async findMovieById(@Param('id') id){

        console.log(id)
        try{
            const movie = await this.movieService.findById(id);
            return movie;
        } catch(e){
            throw new Error("Some bad, get all");
        }
    }

    @Get('title/:title')
    async findMovieByTitle(@Param('title') title){

        console.log(title)
        try{
            const movie = await this.movieService.findByTitle(title);
            return movie;
        } catch(e){
            throw new Error("Some bad, get all");
        }
    }

    
    @Delete('id/:id')
    async deleteMovieById(@Param('id') id){

        console.log(id)
        try{
            const movie = await this.movieService.remove(id);
            return movie;
        } catch(e){
            throw new Error("Some bad, get all");
        }
    }

}
