import { Body, Controller, Get, Param, Post, Put, Query, Req, UseGuards } from '@nestjs/common';
import { AppService } from '../../app.service';
import { UserService } from './../user.service';
import{User}from'../../user/info/entities/user.entity';
import {Request}from'express'
import { AuthGuard } from '@nestjs/passport';
import { AuthService } from "../../auth/auth.service";
@Controller('payment')
export class AccountController {
    constructor(private userService: UserService) {

    }
    
    @Get('/electricity/:ebill')
    @UseGuards(AuthGuard('jwt'))
    async payElectricityBill(@Body() paybillUser: any, @Param('ebill') ebill: number) {
        try {
            await this.userService.payElectricitybill(paybillUser, ebill);
            return `you have paid electricity bill worth Rs. ${ebill}-/`
        } catch (e) {
            throw new Error(e);
        }
    }
    
    @Get('/phone/:phoneBill')
    @UseGuards(AuthGuard('jwt'))
    async payPhoneBill(@Body() paybillUser: any, @Param('phoneBill') phoneBill: number) {
        try {
            await this.userService.payPhonebill(paybillUser, phoneBill);
            return `you have paid phone bill worth Rs. ${phoneBill}-/`
        } catch (e) {
            throw new Error(e);
        }
    }
    
    @Get('/bills')
    @UseGuards(AuthGuard('jwt'))
    async payElectricityAndPhoneBill(@Body() paybillUser: any,@Query() query,) {
      console.log(query)

        try {
            await this.userService.payElectricityAndPhoneBill(paybillUser,query.electricity, query.phone);
            return `you have paid electricity worth Rs. ${query.electricity}-/ and phone bill worth Rs. ${query.phone}-/`
        } catch (e) {
            throw new Error(e);
        }
    }
}
