import { Test, TestingModule } from '@nestjs/testing';
import { AccountController } from './account.controller';
import { UserService } from '../user.service'
import { User } from '../info/entities/user.entity';
import { Repository } from "typeorm";
import { TypeOrmModule } from "@nestjs/typeorm";

describe('AccountController', () => {
  let controller: AccountController;
  let userService: UserService;
  let userRepository: Repository<User>;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [TypeOrmModule.forRoot({
        type: 'mysql',
        host: 'localhost',
        port: 3306,
        username: 'root',
        password: 'pass@word1',
        database: 'bankapp', 
        entities: [User],
        autoLoadEntities: true,
        synchronize: true 
      }),TypeOrmModule.forFeature([User])],
      controllers: [AccountController],
      providers:[UserService]
    }).compile();

    controller = module.get<AccountController>(AccountController);
    userService = new UserService(userRepository)

  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });

  
  it("should  pay electricity bills not greater than balance", async ()=>{
   
    const result:Promise<any> = new Promise((resolve, reject)=>{
      resolve({'emailId':'user4@gmail.com','balance':700})
    });
   
    jest.spyOn(userService,'payElectricitybill').mockImplementation((emailId:string,bill:number)=>result);
    let res = await controller.payElectricityBill({'emailId':'user4@gmail.com'},600);

    expect(res).toBeTruthy()
  })
  it("should  pay phone bills not greater than balance", async ()=>{
   
    const result:Promise<any> = new Promise((resolve, reject)=>{
      resolve({'emailId':'user4@gmail.com','bill':700})
    });
   
    jest.spyOn(userService,'payPhonebill').mockImplementation((emailId:string,bill:number)=>result);
    let res = await controller.payElectricityBill({'emailId':'user4@gmail.com'},600);

    expect(res).toBeTruthy()
  })

  it("should  pay phone bills and electricity bill not greater than balance", async ()=>{
   
    const result:Promise<any> = new Promise((resolve, reject)=>{
      resolve({'emailId':'user4@gmail.com','phonebill':600,'electricitybill':300})
    });
   
    jest.spyOn(userService,'payElectricityAndPhoneBill').mockImplementation((emailId:string,phonebill:number,electricityBill:number)=>result);
    let res = await controller.payElectricityAndPhoneBill({'emailId':'user4@gmail.com'},{'phone':300,'electricity':200});

    expect(res).toBeTruthy()
  })
})
