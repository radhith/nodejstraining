import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { User } from './info/entities/user.entity';
import { Repository } from "typeorm";

@Injectable()
export class UserService {

    constructor(@InjectRepository(User)
    private userRepository:Repository<User>){
        
    }

    saveUser(user:User):Promise<User>{
        return this.userRepository.save(user)
    }

    getBalance(emailId:any):Promise<User>{
        console.log("username"+emailId);
        return this.userRepository.findOneBy({emailId:emailId})
    }

    validateUser(username,password):Promise<User>{
        console.log("user"+username)
        return this.userRepository.findOneBy({username:username,password:password})
    }


   async changePassword(emailId:string,password:string,newpassword:string){
      let user=await  this.userRepository.find({where:{emailId,password}})
      console.log("changepwd"+user)
      if(user.length>0){
      return  this.userRepository.update(emailId,{password:newpassword})

      }else{
        throw new HttpException("Email or password incorrect", HttpStatus.BAD_REQUEST)

      }
    }

    async forgotPassword(email: string,username:string) {
        const validUser = await this.userRepository.find({ where: { emailId:email, username:username } });
        if (validUser.length > 0) {
            return "new password is sent to email id"+email
        }
        else {
            throw new HttpException("Email or password incorrect", HttpStatus.BAD_REQUEST)
        }
    }

    async creditAmountToUser(credit: any) {
        console.log(credit)
        const email = credit.emailId;
        const user = await this.userRepository.find({ where: { emailId:email } });
        if (user.length > 0) {
            const newAmount = user[0].balance + credit.amount
            return await this.userRepository.update(email, { balance: newAmount });
        }
        else {
            throw new HttpException("Email is incorrect", HttpStatus.BAD_REQUEST)
        }
    }
    async payElectricitybill(paybillUser: any, electricityBill: number) {
        const email = paybillUser.emailId
        const validUser = await this.userRepository.find({ where: { emailId:email } });
        if (validUser.length > 0) {
            if (validUser[0].balance > electricityBill) {
                const newAmount = +validUser[0].balance - electricityBill
                return await this.userRepository.update(email, { balance: newAmount });
            } else {
                throw new HttpException("you dont have sufficient balance to pay, please  credit amount", HttpStatus.EXPECTATION_FAILED)
            }
        }
        else {
            throw new HttpException("Email is incorrect", HttpStatus.BAD_REQUEST)

        }
    }

    async payPhonebill(paybillUser: any, phoneBill: number) {
        const email = paybillUser.emailId
        const validUser = await this.userRepository.find({ where: { emailId:email } });
        if (validUser.length > 0) {
            if (validUser[0].balance > phoneBill) {
                const newAmount = +validUser[0].balance - phoneBill
                return await this.userRepository.update(email, { balance: newAmount });
            } else {
                throw new HttpException("you dont have sufficient balance to pay, please  credit amount", HttpStatus.EXPECTATION_FAILED)
            }
        }
        else {
            throw new HttpException("Email is incorrect", HttpStatus.BAD_REQUEST)

        }
    }

    async payElectricityAndPhoneBill(paybillUser: any, electricityBill: Number, phoneBill: Number) {
        const email = paybillUser.emailId
        const validUser = await this.userRepository.find({ where: { emailId:email } });
        if (validUser.length > 0) {
           const billAmount = (+electricityBill) + (+phoneBill)
            if (validUser[0].balance > billAmount) {
                const newAmount = +validUser[0].balance - billAmount
                return await this.userRepository.update(email, { balance: newAmount });
            } else {
                throw new HttpException("you dont have sufficient balance to pay both bill, please credit amount", HttpStatus.EXPECTATION_FAILED)
            }
        }
        else {
            throw new HttpException("Email is incorrect", HttpStatus.BAD_REQUEST)

        }
    }

}
