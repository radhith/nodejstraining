import{Entity,Column,PrimaryGeneratedColumn,PrimaryColumn} from 'typeorm';

@Entity()
export class User{
   
    @PrimaryColumn()
    emailId:string
    @Column()
    username:string
    @Column()
    password:string;

    @Column({default:5000})
    balance:number
} 