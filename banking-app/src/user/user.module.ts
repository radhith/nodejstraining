import { Module } from '@nestjs/common';
import { AccountController } from './account/account.controller';
import { UserService } from './user.service';
import { User } from './info/entities/user.entity';
import { TypeOrmModule } from '@nestjs/typeorm';


@Module({
  imports: [TypeOrmModule.forFeature([User])],
  controllers: [AccountController],
  providers: [UserService],
  exports: [TypeOrmModule, UserService],

})
export class UserModule {}