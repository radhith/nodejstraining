import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { AuthModule } from './auth/auth.module';
import { UserModule } from './user/user.module';
import { UserService } from './user/user.service';
import { ConfigModule } from '@nestjs/config';
import { AuthService } from './auth/auth.service';
import { JwtStrategy } from './auth/jwt.strategy';
import { LocalStrategy } from './auth/local.strategy';
import { JwtService } from '@nestjs/jwt';

@Module({
  imports: [AuthModule, UserModule,
    TypeOrmModule.forRoot({
      type: 'mysql',
      host: 'localhost',
      port: 3306,
      username: 'root',
      password: 'pass@word1',
      database: 'bankapp', 
      entities: [],
      autoLoadEntities: true,
      synchronize: true
    }),
    ConfigModule.forRoot({
      // By default, the package looks for a .env file in the root directory
      envFilePath: '.env',
      ignoreEnvFile: false,
    })],
  controllers: [AppController],
 //providers: [AppService,UserService,AuthService],
 providers:[JwtService,AppService]
})
export class AppModule {}
