import { Injectable } from '@nestjs/common';
import {JwtService} from'@nestjs/jwt';
import { User } from 'src/user/info/entities/user.entity';
import { UserService } from '../user/user.service';

@Injectable()
export class AuthService {

    constructor(private readonly jwtService:JwtService,
        private readonly userService:UserService){

    }

    validateUser(username,password){
        let foundUser=this.userService.validateUser(username,password);
        if(foundUser){
            return foundUser
        }
        return null;
    }
    generateToken(user:User):any{
        
        const {username, emailId} = user;
        const payload = {username, emailId};
        console.log(payload)
        const token = this.jwtService.sign(payload);
        return {token: token};
    }
}
