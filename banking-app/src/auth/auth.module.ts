import { Module } from '@nestjs/common';
import { AuthService } from './auth.service';
import { jwtConstants } from './constants';
import { PassportModule } from '@nestjs/passport';
import { JwtModule, JwtService } from '@nestjs/jwt';
import { JwtStrategy } from './jwt.strategy';
import { LocalStrategy } from './local.strategy';
import{UserModule}from './../user/user.module'
@Module({
  imports:[JwtModule.register({
    signOptions:{
      expiresIn:'1d'
    },
    secret:jwtConstants.secret
  }),
  UserModule,PassportModule],
    providers: [AuthService,JwtStrategy,LocalStrategy],
  exports:[AuthService]
})
export class AuthModule {}
