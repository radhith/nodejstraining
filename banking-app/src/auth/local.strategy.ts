import { Injectable, UnauthorizedException } from "@nestjs/common";
import { PassportStrategy } from "@nestjs/passport";

import{Strategy} from 'passport-local';
import { AuthService } from "./auth.service";
@Injectable()
export class LocalStrategy extends PassportStrategy(Strategy){
    constructor(private readonly _authService: AuthService) {
        super();
      }

      validate(username: string, password: string): any {
      //  validate(payload): any {

        console.log("validate in local "+username)
        const user = this._authService.validateUser(username, password);
        if (!!user) {
            return user;
        }
        throw new UnauthorizedException("User is not valid");
      }
}