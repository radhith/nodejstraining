import { Body, Controller, Get, Param, Post, Put, Req, UseGuards } from '@nestjs/common';
import { AppService } from './app.service';
import { UserService } from './user/user.service';
import{User}from'./user/info/entities/user.entity';
import {Request}from'express'
import { AuthGuard } from '@nestjs/passport';
import { AuthService } from "./auth/auth.service";

@Controller()
export class AppController {
  constructor(private readonly appService: AppService,
    private readonly userService: UserService,
    private readonly authService: AuthService
    ) {}

  @Get()
  getHello(): string {
    return this.appService.getHello();
  }

  @Post('/signup')
  signup(@Body()user:User):any{
    try{
     const users= this.userService.saveUser(user);
     return users

    }catch(e){
      throw new Error('erro')
    }
  }

  @Post('/login')
  @UseGuards(AuthGuard("local"))
  async login(@Body()user:User){
    console.log("inside")
    // try{
    //   let a= await this.userService.validateUser(user);
    //   return a
    // }
    // catch(e){
    //   return 'no user'
    // }

    return this.authService.generateToken(user);
  
  }

  @Get("/balance/:emailId") 
  @UseGuards(AuthGuard("jwt"))
  // @UseGuards(AuthGuard("local"))
  // @UseGuards(LocalAuthGuard)
 async getBalance(@Param('emailId')emailId) {
    try{
      const det= await this.userService.getBalance(emailId);
      return det.balance
 
     }catch(e){
       throw new Error('erro')
     }
  }

  @Post("/changepassword")
  @UseGuards(AuthGuard("jwt"))
 async changePassword(@Body() userDet:any){
  try{
    const det= await this.userService.changePassword(userDet.emailId,userDet.oldpassword,userDet.newpassword);
    return det

   }catch(e){
     throw new Error('erro')
   }
 }

 @Post('/forgotpassword')
 async forgotPassword(@Body() userdet: any) {
     try {
         const forgotPasssword = await this.userService.forgotPassword(userdet.emailId, userdet.username);
         return forgotPasssword;
     } catch (e) {
         throw new Error(e);
     }
 }

 @Put('/credit')
 @UseGuards(AuthGuard('jwt'))
 async creditAmout(@Body()creditAmount:any) {
     try {
         const credit = await this.userService.creditAmountToUser(creditAmount);
         return credit;
     } catch (e) {
         throw new Error(e);
     }
 }

}
