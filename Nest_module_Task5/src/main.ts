import { NestFactory } from '@nestjs/core';
import { NestExpressApplication } from '@nestjs/platform-express';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';
import { join } from 'path';

 import { AppModule } from "./02-modules/app.module";

const bootstrap = async function () {
  // const port = process.env.NODE_PORT||5000;
  const port = 3000;
  const app = await NestFactory.create<NestExpressApplication>(AppModule);

  app.useStaticAssets(join(__dirname + '/../../public'));
  app.setBaseViewsDir(join(__dirname + '/../../views'));

  app.setViewEngine('ejs');

  app.enableCors();



  // swagger
const builder =  new DocumentBuilder()
  .setTitle("SWAGGER")
    .setDescription("This is NestJs with swagger")
    .setVersion("v2.34")
    .setContact("Training Dept", "/", "demo@gmail.com")
  .build();

const config = SwaggerModule.createDocument(app, builder);
  SwaggerModule.setup("api-docs", app, config);

  await app.listen(port);
  console.log('My app is running at ' + port);
};

bootstrap();
