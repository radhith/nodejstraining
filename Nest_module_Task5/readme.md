# Nest
## Install nest js
- $ npm i -g @nestjs/cli

### Create project
- $ nest new nest-tutorial

## Modules: Application or sub-application
#### providers
	re-usable code
	MyMath, MyDate, MyLogger
#### controllers
	express -> routing
	responsible to handle request and redirect to service ->
#### imports
	define children
#### exports
	what this module is exposing


Nest help:
	$ nest --help

To create modules
	- move to src directory or where your *.module.ts is present
	- $ nest g mo module-name